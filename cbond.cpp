#include "cbond.h"

CBond::CBond(QDate issued, long term, long price)
    :issued(issued),term(term),price(price)
{
    maturity = QDate(issued.addYears(term));
    picker = CRatePicker::getPicker();
    picker->setFile("new.xml");

    rate = picker->getRate(issued);
}

QDate CBond::getIssuedDate() const
{
    return issued;
}

QDate CBond::getMaturityDate() const
{
    return maturity;
}

long CBond::getTerm() const
{
    return term;
}

double CBond::getRate() const
{
    return rate;
}

long CBond::getProfitFromSell(const QDate& selling,
                              const long& sellPrice) const
{
    QDate cut(selling);
    if (selling < issued)
        cut = issued;
    if (selling > maturity)
        cut = maturity;

    long profit = sellPrice - price;
    QDate temp(issued.addMonths(6));
    while (temp <= cut)
    {
        profit += (long)( price * (double)rate/200 );
        temp = temp.addMonths(6);
    }
    return profit;
}

long CBond::getProfitFromBuy(const QDate &buying,
                             const long& buyPrice) const
{
    QDate cut(buying);
    if (buying < issued)
        cut = issued;
    if (buying > maturity)
        cut = maturity;

    long profit = price - buyPrice;
    QDate temp(maturity);
    while (temp > cut)
    {
        profit += (long)( price * (double)rate/200 );
        temp = temp.addMonths(-6);
    }
    return profit;
}
