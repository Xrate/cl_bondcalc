#-------------------------------------------------
#
# Project created by QtCreator 2014-09-09T18:31:46
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = bonds
TEMPLATE = app
RESOURCES += resources.qrc

SOURCES += main.cpp\
        mainwindow.cpp \
    cbond.cpp \
    cratepicker.cpp

HEADERS  += mainwindow.h \
    cbond.h \
    cratepicker.h

FORMS    += mainwindow.ui
