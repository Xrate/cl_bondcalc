#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    connect ( ui->pushButton, SIGNAL( clicked() ), this, SLOT( calculate() ) );
}

void MainWindow::calculate()
{
    ui->sellerprofit->setText("");
    ui->buyerprofit->setText("");
    ui->ratedit->setText("");
    ui->erroredit->setText("");

    QStringList list = ui->bonddate->text().split("-");
    list.removeAll("");
    if(list.length() != 3)
        return;
    QDate issued(list.at(2).toInt(),
                 list.at(1).toInt(),
                 list.at(0).toInt());

    list = ui->selldate->text().split("-");
    list.removeAll("");
    if(list.length() != 3)
        return;
    QDate deal(list.at(2).toInt(),
               list.at(1).toInt(),
               list.at(0).toInt());

    if (deal < issued)
    {
        ui->erroredit->setText("Issue date must be earlier than deal date!");
        return;
    }

    long term = ui->termedit->text().toInt()/12;
    if (term < 0)
    {
        ui->erroredit->setText("Incorrect term!");
        return;
    }
    if (deal > issued.addYears(term))
    {
        ui->erroredit->setText("Maturity date must be later than deal date!");
        return;
    }

    long price = ui->priceedit->text().toDouble()*100;
    if (price < 0)
    {
        ui->erroredit->setText("Incorrect price!");
        return;
    }

    double discount = ui->discountedit->text().toDouble();
    if (discount < 0 || discount > 100)
    {
        ui->erroredit->setText("Incorrect discount (0-100)!");
        return;
    }

    CBond bond(issued,term,price);
    long profitS = bond.getProfitFromSell(deal,price*(100-discount)/100);
    long profitB = bond.getProfitFromBuy(deal,price*(100-discount)/100);
    double rate = bond.getRate();
    if (rate < 0)
    {
        ui->erroredit->setText("No info about interest rate at this date!");
        return;
    }

    ui->sellerprofit->setText(QString::number(profitS/100));
    ui->buyerprofit->setText(QString::number(profitB/100));
    ui->ratedit->setText(QString::number(rate));
}

MainWindow::~MainWindow()
{
    delete ui;
}
