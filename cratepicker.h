#ifndef CRATEPICKER_H
#define CRATEPICKER_H

#include <QFile>
#include <QXmlStreamReader>
#include <QStringList>
#include <QDate>
#include <QDebug>
#include <QFile>

class CRatePicker
{
public:
    static CRatePicker* getPicker();

    double getRate(const QDate&);
    void setFile(QString filename);
private:
    static bool alive;
    static CRatePicker* singleton;

    CRatePicker(){}
    ~CRatePicker(){}
    QString fileName;
};

#endif // CRATEPICKER_H
