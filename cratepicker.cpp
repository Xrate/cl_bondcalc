#include "cratepicker.h"
#include <iostream>

bool CRatePicker::alive = 0;
CRatePicker* CRatePicker::singleton = 0;

CRatePicker* CRatePicker::getPicker()
{
    if (!CRatePicker::singleton)
        CRatePicker::singleton = new CRatePicker();
    return CRatePicker::singleton;
}

void CRatePicker::setFile(QString filename)
{
    fileName = filename;
}


double CRatePicker::getRate(const QDate& date)
{
    double rate = -1.;
    QString dateStr = date.toString("yyyy-MM-dd");
    dateStr.remove(dateStr.length()-1,1);
    QString date_temp;

    QFile* file = new QFile(fileName);
    if (!file->open(QIODevice::ReadOnly | QIODevice::Text))
    {
        std::cout << "Cannot find file new.xml with database" << std::endl;
        exit(EXIT_FAILURE);
    }
    QXmlStreamReader *inputStream = new QXmlStreamReader(file);

    bool found = 0;

    while (!inputStream->atEnd() )
    {
        QXmlStreamReader::TokenType token = inputStream->readNext();
        if (token == QXmlStreamReader::StartDocument)
            continue;
        if (token == QXmlStreamReader::StartElement)
        {
            if (inputStream->name() == "Cube")
            {
                date_temp = inputStream->attributes().value("TIME").toString();

                if (date_temp.startsWith(dateStr))
                {
                    found = 1;
                }
                if (found == 1)
                {
                    QStringList list = date_temp.split("-");
                    list.removeAll("");
                    if(list.length() != 3)
                        continue;
                    QDate dateC(list.at(0).toInt(),
                                list.at(1).toInt(),
                                list.at(2).toInt());

                    if (date.daysTo(dateC) >= 0 && date.daysTo(dateC) < 5 )
                    {
                        rate = inputStream->attributes().value("OBS_VALUE").toString().toDouble();
                        break;
                    }
                }
            }
        }
    }
    file->close();
    inputStream->clear();
    delete inputStream;
    delete file;
    return rate;
}
