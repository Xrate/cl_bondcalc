#ifndef CBOND_H
#define CBOND_H

#include <QDate>
#include <QFile>
#include <QXmlStreamReader>
#include "cratepicker.h"

class CBond
{
public:
    CBond(QDate issued, long term, long price);

    QDate getIssuedDate() const;
    QDate getMaturityDate() const;
    long getTerm() const;
    double getRate() const;
    long getProfitFromSell(const QDate& selling, const long& price) const;
    long getProfitFromBuy(const QDate& buying, const long& price) const;

private:
    CRatePicker *picker;
    QDate issued;
    QDate maturity;
    long term;
    long price;
    double rate;
};

#endif // CBOND_H
